const startServer = require("universal-webpack/server");
const settings = require("../../webpack/universal-webpack-settings");
// `configuration.context` and `configuration.output.path` are used
const configuration = require("../../webpack/webpack.config");

startServer(configuration, settings);
