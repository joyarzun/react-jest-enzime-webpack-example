import fs from "fs";
import path from "path";
import React from "react";
import express from "express";
import { renderToString } from "react-dom/server";
import providerApp from "../shared/providerApp";
import getData from "../shared/getData";

export default parameters => {
  // Create HTTP server
  const app = new express();

  // Serve static files
  app.use(express.static(path.join(__dirname, "../client"), { index: false }));

  // Proxy API calls to API server
  // const proxy = http_proxy.createProxyServer({
  //   target: "http://localhost:xxxx"
  // });
  // app.use("/api", (req, res) => proxy.web(req, res));

  // React application rendering
  app.use((req, res) => {
    if (req.originalUrl == "/") {
      getData().then(data => {
        res.status(200);
        res.send(
          htmlBundled.replace(
            `<div id="root"></div>`,
            `<div id="root">${renderToString(providerApp(data))}</div>`
          )
        );
      });
    }
  });

  app.listen(3000, () => console.log("Example app listening on port 3000!"));
};

const htmlBundled = fs
  .readFileSync(path.join(__dirname, "../client", "index.html"))
  .toString();
