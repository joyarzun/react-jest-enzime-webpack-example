import React from "react";

const Counter = ({ value, onIncrement, onDecrement }) => (
  <div className="row">
    <div className="col">
      Clicked: {value} times <button onClick={onIncrement}>+</button>{" "}
      <button onClick={onDecrement}>-</button>
    </div>
  </div>
);

export default Counter;
