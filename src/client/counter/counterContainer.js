import { connect } from "react-redux";
import { increment, decrement } from "./actions";
import Counter from "./Counter";

const mapStateToProps = state => {
  return {
    value: state.counterCalc
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onIncrement: () => {
      dispatch(increment(2));
    },
    onDecrement: () => {
      dispatch(decrement(2));
    }
  };
};

const counterContainer = connect(mapStateToProps, mapDispatchToProps)(Counter);

export default counterContainer;
