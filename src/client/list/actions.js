export const CHANGE = "CHANGE";

export const change = list => {
  return { type: CHANGE, list };
};
