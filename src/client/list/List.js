import React from "react";
import styles from "../../shared/App.css";

const Elem = props => (
  <div className="row">
    <div className="col">
      <p>{props.name}</p>
    </div>
  </div>
);

const List = props =>
  props.list.map((name, idx) => <Elem key={idx} name={name} />);

export default List;
