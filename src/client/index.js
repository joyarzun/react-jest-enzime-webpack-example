import React from "react";
import ReactDOM from "react-dom";
import providerApp from "../shared/providerApp";
import getData from "../shared/getData";

getData().then(data => {
  ReactDOM.hydrate(providerApp(data), document.getElementById("root"));
});
