import { INCREMENT, DECREMENT } from "../client/counter/actions";
import { CHANGE } from "../client/list/actions";
import { combineReducers } from "redux";

const counterCalc = (state = 0, action) => {
  switch (action.type) {
    case INCREMENT:
      return state + action.size;
    case DECREMENT:
      return state - action.size;
    default:
      return state;
  }
};

const changeList = (state = [], action) => {
  switch (action.type) {
    case CHANGE:
      return action.list;
    default:
      return state;
  }
};

const reducer = combineReducers({
  counterCalc,
  changeList
});

export default reducer;
