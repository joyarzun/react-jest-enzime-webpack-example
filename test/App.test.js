import React from "react";
import { shallow } from "enzyme";

import App from "../src/shared/App";
import Title from "../src/client/Title";
import List from "../src/client/list/listContainer";
import Counter from "../src/client/counter/counterContainer";

describe("<App />", () => {
  it("renders <App /> components", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(Title).length).toBe(1);
    expect(wrapper.find(List).length).toBe(1);
    expect(wrapper.find(Counter).length).toBe(1);
  });
});
