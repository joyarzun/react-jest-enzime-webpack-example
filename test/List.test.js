import React from "react";
import { mount } from "enzyme";

import List from "../src/client/list/List";

describe("<List />", () => {
  it("renders <List /> components", () => {
    const wrapper = mount(<List list={["bar", "foo", "baz"]} />);
    expect(wrapper.find(".row").length).toBe(3);
  });
});
