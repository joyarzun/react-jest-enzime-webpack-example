import React from "react";
import { mount } from "enzyme";

import Counter from "../src/client/counter/Counter";

describe("<Counter />", () => {
  let wrapper;
  const props = { value: 1, onIncrement: jest.fn(), onDecrement: jest.fn() };
  beforeEach(() => {
    wrapper = mount(<Counter {...props} />);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it("renders <Counter /> components", () => {
    expect(wrapper.find(".row").length).toBe(1);
    expect(wrapper.text()).toMatch("Clicked: 1 times");
  });

  it("call increment funcion when user do click", () => {
    wrapper
      .find("button")
      .findWhere(x => x.html() && x.text() === "+")
      .simulate("click");
    expect(props.onIncrement.mock.calls.length).toBe(1);
  });

  it("call decrement funcion when user do click", () => {
    wrapper
      .find("button")
      .findWhere(x => x.html() && x.text() === "-")
      .simulate("click");
    expect(props.onDecrement.mock.calls.length).toBe(1);
  });
});
